## Introducing 👋
Hello! My name is Artur, and I work as a Backend Developer.

## Key Points ⚡
- Key Skill: Java
- English Proficiency: Intermediate
- Current Focus: Working on several pet projects
- Soft Skills: Sociable, positive, responsible.

## Technical Stack 🛠
- Java
- SQL
- Build Tools: Maven, Gradle
- Frameworks: Spring Framework
- ORM: Hibernate
- Lombok
- Version Control: Git

## Education 💬
- Bachelor's Degree: KNITU KAI im. Tupolev in Management
- Master's Degree: KNITU KAI im. Tupolev in Information Technology
- Professional Certification: Java Backend Developer from Maxima IT School
- Additional Courses: Stepik.org Python courses

## My Projects
- [Telegram Bot](https://gitlab.com/drygkollektiv/laligafantasybot) Technologies: Spring Boot, Scheduler, HTML Parser, JSOUP, Jackson
- [Library](https://gitlab.com/drygkollektiv/library) Features: Pagination, Spring Data JPA, Thymeleaf
- [IT School](https://gitlab.com/drygkollektiv/43-vlasov-project) Compilation of various projects from the IT School

Feel free to explore the repositories for more details on each project. I am enthusiastic about new challenges and eager to bring my skills to a dynamic team.
